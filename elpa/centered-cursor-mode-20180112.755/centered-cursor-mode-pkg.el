;;; -*- no-byte-compile: t -*-
(define-package "centered-cursor-mode" "20180112.755" "cursor stays vertically centered" 'nil :commit "319636448ffb7dba5fade3b2599ed9c1fd3bf8c8" :url "https://github.com/andre-r/centered-cursor-mode.el" :keywords '("convenience"))
