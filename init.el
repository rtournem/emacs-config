;;; package --- Summary
;;; Commentary:
(require 'package)
;;; Code:
;; configuration MELPA
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)

;;automatiquement créé par emacs
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-fold-macro-spec-list
   (quote
    (("[f]"
      ("footnote" "marginpar"))
     ("[c]"
      ("cite" "citep"))
     ("[l]"
      ("label"))
     ("[r]"
      ("ref" "pageref" "eqref"))
     ("[i]"
      ("index" "glossary"))
     ("[1]:||*"
      ("item"))
     ("..."
      ("dots"))
     ("(C)"
      ("copyright"))
     ("(R)"
      ("textregistered"))
     ("TM"
      ("texttrademark"))
     (1
      ("part" "chapter" "section" "subsection" "subsubsection" "paragraph" "subparagraph" "part*" "chapter*" "section*" "subsection*" "subsubsection*" "paragraph*" "subparagraph*" "emph" "textit" "textsl" "textmd" "textrm" "textsf" "texttt" "textbf" "textsc" "textup")))))
 '(ansi-color-names-vector
   ["#3F3F3F" "#CC9393" "#7F9F7F" "#F0DFAF" "#8CD0D3" "#DC8CC3" "#93E0E3" "#DCDCCC"])
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(custom-safe-themes
   (quote
    ("599f1561d84229e02807c952919cd9b0fbaa97ace123851df84806b067666332" "88c9585322c217e9ef986050092c010c11f31d39db62e7adc369e5b4376266d4" default)))
 '(elpy-project-root-finder-functions
   (quote
    (elpy-project-find-projectile-root elpy-project-find-python-root)))
 '(elpy-rpc-python-command "python3")
 '(elpy-rpc-pythonpath "/Users/robintournemenne/.emacs.d/elpa/elpy-20181018.2325/")
 '(elpy-shell-display-buffer-after-send t)
 '(elpy-shell-echo-input nil)
 '(elpy-shell-echo-input-cont-prompt nil)
 '(elpy-shell-echo-output nil)
 '(elpy-shell-use-project-root nil)
 '(fci-rule-color "#383838")
 '(minimap-automatically-delete-window nil)
 '(minimap-window-location (quote right))
 '(nrepl-message-colors
   (quote
    ("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3")))
 '(package-selected-packages
   (quote
    (flymd magit yaml-mode smooth-scroll smooth-scrolling sublimity elpy company gruvbox-theme leuven-theme markdown-mode pdf-tools yafolding auto-complete-auctex wcheck-mode writegood-mode auctex centered-cursor-mode buffer-move matlab-mode isend-mode highlight-indent-guides wakatime-mode tabbar zoom helm disable-mouse flycheck exec-path-from-shell jedi zenburn-theme hardcore-mode smart-tabs-mode)))
 '(pdf-info-log nil)
 '(pdf-view-midnight-colors (quote ("#DCDCCC" . "#383838")))
 '(python-shell-exec-path nil)
 '(python-shell-extra-pythonpaths
   (quote
    ("/Users/robintournemenne/REPO/INRIA/WORK/OPENWIND/")))
 '(pyvenv-workon "testMKL3")
 '(smooth-scroll/vscroll-step-size 8)
 '(vc-annotate-background "#2B2B2B")
 '(vc-annotate-color-map
   (quote
    ((20 . "#BC8383")
     (40 . "#CC9393")
     (60 . "#DFAF8F")
     (80 . "#D0BF8F")
     (100 . "#E0CF9F")
     (120 . "#F0DFAF")
     (140 . "#5F7F5F")
     (160 . "#7F9F7F")
     (180 . "#8FB28F")
     (200 . "#9FC59F")
     (220 . "#AFD8AF")
     (240 . "#BFEBBF")
     (260 . "#93E0E3")
     (280 . "#6CA0A3")
     (300 . "#7CB8BB")
     (320 . "#8CD0D3")
     (340 . "#94BFF3")
     (360 . "#DC8CC3"))))
 '(vc-annotate-very-old-color "#DC8CC3")
 '(wakatime-cli-path "/usr/local/bin/wakatime")
 '(wakatime-python-bin nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;chargement du thème
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(load-theme 'zenburn t)
;; (load-theme 'leuven t)
;; (load-theme 'gruvbox-light-soft t)

;;essentiel
;; spécialement pour mac, il faut le module exec-path...
(add-to-list 'load-path "~/elisp")
(require 'exec-path-from-shell)
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))
;; petit morceau de code qui permet de gérer la sauvegarde des
;; fichiers en tant que fichiers exécutables
(add-hook 'after-save-hook
	  #'(lambda ()
	      (and (save-excursion
		     (save-restriction
		       (widen)
		       (goto-char (point-min))
		       (save-match-data
			 (looking-at "^#!"))))
		   (not (file-executable-p buffer-file-name))
		   (shell-command (concat "chmod u+x " buffer-file-name))
		   (message
		    (concat "Saved as script: " buffer-file-name)))))

;; de l'ergonomie
(setq ring-bell-function 'ignore)
(setq mac-right-option-modifier 'none)
(setq ns-function-modifier 'hyper)
(global-auto-revert-mode t)
(desktop-save-mode 1)
(savehist-mode 1)
;; ;; peut être à creuser, c'était surement pour tenter d'avoir
;; ;; l'interpréteur tout le temps dans un buffer fixe
;; (defun toggle-window-dedicated ()
;;   ;; "Control whether or not Emacs is allowed to display another
;;   ;; buffer in current window."
;;   (interactive)
;;   (message
;;    (if (let (window (get-buffer-window (current-buffer)))
;;          (set-window-dedicated-p window (not (window-dedicated-p window))))
;;        "%s: Can't touch this!"
;;      "%s is up for grabs.")
;;    (current-buffer)))
;; (global-set-key (kbd "C-c t") 'toggle-window-dedicated)
;; (require 'yafolding)
;; ;; (define-key yafolding-mode-map (kbd "<C-S-return>") nil)
;; ;; (define-key yafolding-mode-map (kbd "<C-M-return>") nil)
;; ;; (define-key yafolding-mode-map (kbd "<C-return>") nil)
;; ;; (define-key yafolding-mode-map (kbd "C-H-return <C-M-return>") 'yafolding-toggle-all)
;; ;; (define-key yafolding-mode-map (kbd "C-c <C-S-return>") 'yafolding-hide-parent-element)
;; ;; (define-key yafolding-mode-map (kbd "C-H-a <C-return>") 'yafolding-toggle-element)
;; ;; (add-hook 'python-mode
;;           ;; (lambda () (yafolding-mode)))

;; de l'indentation et de la disposition du contenu
(setq-default tab-width 2)
(smart-tabs-insinuate 'c++ 'java 'python)
;;je supprime le caractère tab (une pression de la touche tab insert
;;maintenant des espaces) pour mieux le définir par la suite
(setq-default indent-tabs-mode nil)
(smart-tabs-advice python-indent-line-1 python-indent)
(add-hook 'python-mode-hook
	  (lambda ()
	    (setq indent-tabs-mode t)
	    (setq tab-width (default-value 'tab-width))))
(setq fill-column 80)
;; le retour à la ligne automatique
;; (add-hook 'python-mode-hook 'turn-on-auto-fill)
(add-hook 'python-mode-hook (lambda ()
                             (auto-fill-mode 1)
                             (setq fill-column 80)))
(add-hook 'text-mode-hook 'turn-on-auto-fill)
(add-hook 'emacs-lisp-mode-hook 'turn-on-auto-fill)

(add-hook 'LaTeX-mode-hook (lambda ()
                             (auto-fill-mode 1)
                             (setq fill-column 80)))
;; gestion du bug du linum-mode avec
;; l'augmentation de la taille des caractères
(require 'linum)
(defun linum-update-window-scale-fix (win)
  (set-window-margins win
          (ceiling (* (if (boundp 'text-scale-mode-step)
                  (expt text-scale-mode-step
                    text-scale-mode-amount) 1)
              (if (car (window-margins))
                  (car (window-margins)) 1)
              ))))
(advice-add #'linum-update-window :after #'linum-update-window-scale-fix)
;;gérer le mouvement des contenu selon les fenêtres
(require 'buffer-move)
(global-set-key (kbd "C-H-p")     'buf-move-up)
(global-set-key (kbd "C-H-n")   'buf-move-down)
(global-set-key (kbd "C-H-b")   'buf-move-left)
(global-set-key (kbd "C-H-f")  'buf-move-right)
;;changer l'orientation d'une sparation
(defun toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
         (next-win-buffer (window-buffer (next-window)))
         (this-win-edges (window-edges (selected-window)))
         (next-win-edges (window-edges (next-window)))
         (this-win-2nd (not (and (<= (car this-win-edges)
                     (car next-win-edges))
                     (<= (cadr this-win-edges)
                     (cadr next-win-edges)))))
         (splitter
          (if (= (car this-win-edges)
             (car (window-edges (next-window))))
          'split-window-horizontally
        'split-window-vertically)))
    (delete-other-windows)
    (let ((first-win (selected-window)))
      (funcall splitter)
      (if this-win-2nd (other-window 1))
      (set-window-buffer (selected-window) this-win-buffer)
      (set-window-buffer (next-window) next-win-buffer)
      (select-window first-win)
      (if this-win-2nd (other-window 1))))))

(global-set-key (kbd "C-x |") 'toggle-window-split)

;;de la réalité augmentée (ajouts visuels)
(global-linum-mode)
(show-paren-mode 1)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 5))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1)
;; (require 'minimap-mode)
;; (minimap-mode t)
;; ;;;###autoload

;; (defun minimap-toggle ()
;;   "Toggle minimap for current buffer."
;;   (interactive)
;;   (if (null minimap-buffer-name)
;;       (minimap-create)
;;     (minimap-kill)))

(setq column-number-mode t)
(require 'fill-column-indicator)
(setq fci-rule-column 80)
;; global fci-mode (pas recommandé notamment pour ma visu de pdf
;; (define-globalized-minor-mode global-fci-mode fci-mode (lambda () (fci-mode 1)))
;; (global-fci-mode 1)

;; fci pour python et tex et matlab
(add-hook 'python-mode-hook (lambda () (fci-mode 1)))
(add-hook 'LaTeX-mode-hook (lambda () (fci-mode 1)))
(add-hook 'matlab-mode-hook (lambda () (fci-mode 1)))

;; (require 'column-marker)
;; (add-hook 'python-mode-hook (lambda () (interactive) (column-marker-1 79)))

;; le checking des erreurs! de syntaxe
(add-hook 'after-init-hook #'global-flycheck-mode)
;; le paramètrage de helm
(require 'helm-config)
(require 'helm)
(setq helm-mode t)
(global-set-key (kbd "M-x") #'helm-M-x)
(global-set-key (kbd "C-x r b") #'helm-filtered-bookmarks)
(global-set-key (kbd "C-x C-f") #'helm-find-files)
(global-set-key (kbd "C-c h") 'helm-command-prefix)
(global-unset-key (kbd "C-x c"))

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

(when (executable-find "curl")
  (setq helm-google-suggest-use-curl-p t))

(setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
      helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
      helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
      helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
      helm-ff-file-name-history-use-recentf t)
;; l'application de visualisation de mon activité de codeur
(global-wakatime-mode)
(defvar wakatime-api-key)
(setq wakatime-api-key "19918a0a-93a4-49e3-b005-d282b2d16a5a")
;; ajoute les colonnes grisées pour bien repérer l'indentation
(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
(defvar highlight-indent-guides-method)
(setq highlight-indent-guides-method 'column)
(global-hl-line-mode)
(setq global-hl-line-sticky-flag t)

;; ;; je veux voir la fin des lignes des terminaux !!!!
;; ;; voici le code qui le fait:
;; ;alist of 'buffer-name / timer' items
;; (defvar buffer-tail-alist nil)
;; (defun buffer-tail (name)
;;   "follow buffer tails"
;;   (cond ((or (equal (buffer-name (current-buffer)) name)
;;          (string-match "^ \\*Minibuf.*?\\*$" (buffer-name (current-buffer)))))
;;         ((get-buffer name)
;;       (with-current-buffer (get-buffer name)
;;         (goto-char (point-max))
;;         (let ((windows (get-buffer-window-list (current-buffer) nil t)))
;;           (while windows (set-window-point (car windows) (point-max))
;;          (with-selected-window (car windows) (recenter -3)) (setq windows (cdr windows))))))))

;; (defun toggle-buffer-tail (name &optional force)
;;   "Toggle tailing of buffer NAME.
;; when called non-interactively, a
;; FORCE arg of 'on' or 'off' can be used to to ensure a given state
;; for buffer NAME"
;;   (interactive (list (cond ((if name name) (read-from-minibuffer
;;       (concat "buffer name to tail"
;;         (if buffer-tail-alist (concat " (" (caar buffer-tail-alist) ")") "") ": ")
;;     (if buffer-tail-alist (caar buffer-tail-alist)) nil nil
;;            (mapcar '(lambda (x) (car x)) buffer-tail-alist)
;;         (if buffer-tail-alist (caar buffer-tail-alist)))) nil)))
;;   (let ((toggle (cond (force force) ((assoc name buffer-tail-alist) "off") (t "on")) ))
;;     (if (not (or (equal toggle "on") (equal toggle "off")))
;;       (error "Invalid 'force' arg.  required 'on'/'off'")
;;       (progn
;;         (while (assoc name buffer-tail-alist)
;;            (cancel-timer (cdr (assoc name buffer-tail-alist)))
;;            (setq buffer-tail-alist (remove* name buffer-tail-alist :key 'car :test 'equal)))
;;         (if (equal toggle "on")
;;             (add-to-list 'buffer-tail-alist (cons name (run-at-time t 1 'buffer-tail name))))
;;         (message "toggled 'tail buffer' for '%s' %s" name toggle)))))
;; ;; à voir si je le rajoute pas avec une expression régulière pour celui qui s'ouvre
;; ;; avec realgud
;; (toggle-buffer-tail "*Python*" "on")

;; du debuggage (décommenter les 5 lignes (jusqu'à (require'isend-mode
;; si besoin de realgud)
(load-library "realgud")
(defvar realgud-safe-mode)
(setq realgud-safe-mode' nil)
;;le mode de la vie qui envoie les commandes directement dans l'interpréteur!
(add-to-list 'load-path "~/.emacs.d/elpa/isend-mode-20171118.745/isend-mode")
(require 'isend-mode)


;; selon moi ceci est unf fonction pour lancer proprement
;; une console ipython à partir d'une console python (en fait je ne
;; m'en sers pas, c'est pourri)
;; (defun python-interactive ()
;;   "Enter the interactive Python environment"
;;   (interactive)
;;   (progn
;;     (insert "!import IPython; IPython.start_ipython()")
;;     (move-end-of-line 1)
;;     (comint-send-input)
;;     (insert "whos")
;;     (move-end-of-line 1)
;;     (comint-send-input)
;;     (insert "exit")
;;     (move-end-of-line 1)
;;     (comint-send-input)))
;; (global-set-key (kbd "C-c i") 'python-interactive)

;; use to be able to access the numpy of the mkl compiled numpy
(setenv "LD_LIBRARY_PATH"
  (concat
   "/usr/local/bin" ":"
   "/opt/intel/compilers_and_libraries_2019/mac/mkl/lib" ":"
   "/opt/intel/compilers_and_libraries_2019/mac/lib"
  )
)
(elpy-enable)
;; (setq python-shell-interpreter "jupyter"
;;       python-shell-interpreter-args "console --simple-prompt"
;;       python-shell-prompt-detect-failure-warning nil)
;; (add-to-list 'python-shell-completion-native-disabled-interpreters
;;              "jupyter")

(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "-i --simple-prompt")

;; (setq python-shell-interpreter "python"
;;       python-shell-interpreter-args "-i")

;; ;; de la gestion de python
;; (defvar python-shell-interpreter)
;; (defvar python-shell-interpreter-args);
;; ;; (setq python-shell-interpreter "python3")
;; ;; la je donne des arguments au lancement de la console ypython le
;; ;; simple-prompt et pylab
;; ;; (setq python-shell-interpreter "ipython"
;; ;;       python-shell-interpreter-args "--simple-prompt -i --pylab")
;; (setq python-shell-interpreter "ipython"
;;       python-shell-interpreter-args "--simple-prompt")
;; ;; l'autocomplétion pour python! avec les bulles d'aide et
;; ;; l'aide pour al signature
;; (add-hook 'python-mode-hook 'jedi:setup)
;; (defvar jedi:complete-on-dot)
;; (setq jedi:complete-on-dot t)
;; ;; ligne pour essayer de gérer la fin des lignes de commentaire en python (ça fait buguer le mode fondamental)
;; ;; (defun odd-number-of-single-quotes-this-paragraph-so-far ()
;; ;;   (oddp (how-many "'" (save-excursion (backward-paragraph) (point)) (point))))
;; ;; (defun odd-number-of-double-quotes-this-paragraph-so-far ()
;; ;;   (oddp (how-many "\"" (save-excursion (backward-paragraph) (point)) (point))))

;; ;; (add-to-list 'fill-nobreak-predicate
;; ;;      'odd-number-of-single-quotes-this-paragraph-so-far)
;; ;; (add-to-list 'fill-nobreak-predicate
;; ;;              'odd-number-of-double-quotes-this-paragraph-so-far)

;; ;; de la gestion de Matlab
;; ;; malheureusement le matlab mode est pourri
;; ;; Replace path below to be where your matlab.el file is.
;; ;;(add-to-list 'load-path "~/path/to/matlab-emacs")
;; ;;(load-library "matlab-load")

(server-start)
(require 'matlab)
(defvar matlab-mode)
(setq matlab-mode t)

;; Enable CEDET feature support for MATLAB code. (Optional)
;; (matlab-cedet-setup)
;; (add-hook 'matlab-mode-hook 'auto-complete-mode)
;; (setq auto-mode-alist
      ;; (cons
       ;; '("\\.m$" . matlab-mode)
       ;; auto-mode-alist))
;; (add-hook 'matlab-mode
          ;; (lambda ()
            ;; (auto-complete-mode 1)
            ;; ))

;;de la gestion de LaTeX (à documenter...)
(require 'auto-complete);Ceci a corrigé l'erreur ac-mode returns
                        ;void... I have no idea what I am doing!
(setq-default TeX-master nil)
(setq TeX-parse-self t)
(setq TeX-auto-save t)

(add-hook 'LaTeX-mode-hook (lambda ()
                             (TeX-fold-mode 1)))
(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)
(add-hook 'TeX-language-dk-hook
          (lambda () (ispell-change-dictionary "english")))

;; je l'ai remis comme le conseillais le tuto piotrkazmierczak mais je ne
;; sais pas si c'est instable, on va voir avec la suite
;; (setq ispell-program-name "aspell") ; could be ispell as well, depending on your preferences
;; (setq ispell-dictionary "french") ; this can obviously be set to any language your spell-checking program supports
;; (setq ispell-dictionary "english") ; this can obviously be set to any language your spell-checking program supports
;; (add-hook 'LaTeX-mode-hook 'flyspell-mode)

;; je crois que ça test le buffer d'entrée de jeu et ça je ne veux pas
;; car ça fait hang emacs...
;; (add-hook 'LaTeX-mode-hook 'flyspell-buffer)

;; parmi les tests permettant de vérifier l'orthographe en temps réel
;; (setq wcheck-language-data
;;       '(("francais"
;;          (program . "/usr/local/Cellar/hunspell/1.6.2/bin/hunspell")
;;          (args "-l" "-d" "fr-moderne")
;;          (action-program . "/usr/local/Cellar/hunspell/1.6.2/bin/hunspell")
;;          (action-args "-a" "-d" "fr-moderne")
;;          (action-parser . wcheck-parser-ispell-suggestions))))
;;; Spell checking using hunspell
;; (setq ispell-dictionary-alist
;;   '((nil "[A-Za-z]" "[^A-Za-z]" "[']" t
;;      ("-d" "en_US" "-i" "utf-8") nil utf-8)
;;     ("american"
;;      "[A-Za-z]" "[^A-Za-z]" "[']" nil
;;      ("-d" "en_US") nil utf-8)
;;     ("english"
;;      "[A-Za-z]" "[^A-Za-z]" "[']" nil
;;      ("-d" "en_GB") nil utf-8)
;;     ("french"
;;      "[A-Za-z]" "[^A-Za-z]" "[']" nil
;;      ("-d" "fr") nil utf-8)
;;     ("british"
;;      "[A-Za-z]" "[^A-Za-z]" "[']" nil
;;      ("-d" "en_GB") nil utf-8)
;;     ("norsk"
;;      "[A-Za-zÉÆØÅéæøå]" "[^A-Za-zÉÆØÅéæøå]" "[\"]" nil
;;      ("-d" "nb_NO") "~list" utf-8)))
;; (eval-after-load "ispell"
;;   (progn
;;     (setq ispell-dictionary "french"
;;           ispell-extra-args '("-a" "-i" "utf-8")
;;           ispell-silently-savep t)))
;; (setq-default ispell-program-name "hunspell")

;; ligne pour forcer la compilation de fcichier pdf par l'utilisation
;; de la commande pdflatex et pas latex tout court (qui créerait alors
;; surement qu'un dvi)
(setq TeX-PDF-mode t)
;; je me demande s'il n'y a pas de problèmes pour les fichiers beamer,
;; dans ce cas il faut le lancer à la mano: M-x TeX-PDF-mode

(require 'reftex)
;; permet d'ajouter l'environnement subnumcases à reftex pour les
;; équations le problème est qu'il me rajoute des parenthèses.... à corriger.
(add-to-list 'reftex-label-alist
             '("subnumcases" 101 "eq:" nil eqnarray-like))
;; '("subnumcases" 101 nil "~\\eqref{%s}" eqnarray-like))
;; ("subnumcases" 101 "eq:" nil eqnarray-like)

(setq TeX-source-correlate-mode t)
;; (setq TeX-command-extra-options "--synctex=1")

;; pour gérer synctex
(define-advice pdf-info-process-assert-running (:around (fn &optional force))
  (let ((process-environment
         (cons "LC_NUMERIC=C" process-environment)))
    (funcall fn force)))


;; la ligne suivante permet à pdf-tools de ne pas prendre 10 secondes
(setq pdf-view-use-unicode-ligther nil)
(pdf-tools-install)
;; (add-hook 'doc-view-mode-hook 'pdf-tools-install)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)   ; with AUCTeX LaTeX mode
;; (add-hook 'latex-mode-hook 'turn-on-reftex)   ; with Emacs latex mode
;; Use pdf-tools to open PDF files
(setq TeX-view-program-selection '((output-pdf "PDF Tools"))
      TeX-view-program-list '(("PDF Tools" TeX-pdf-tools-sync-view))
      TeX-source-correlate-start-server t)
;; Update PDF buffers after successful LaTeX runs
(add-hook 'TeX-after-compilation-finished-functions
          #'TeX-revert-document-buffer)

;; pour désactiver le linum mode en pdf mode
;; ancienne version 
;; (add-hook 'pdf-view-mode-hook (lambda() (linum-mode -1)))
;; nouvelle version plus versatile prise d'internet
(setq linum-mode-inhibit-modes-list '(pdf-view-mode
                                      doc-view-mode))

(defadvice linum-on (around linum-on-inhibit-for-modes)
  "Stop the load of linum-mode for some major modes."
    (unless (member major-mode linum-mode-inhibit-modes-list)
      ad-do-it))

(ad-activate 'linum-on)

;; tentative de code folding encore infructueuse
(defun turn-on-outline-minor-mode ()
  (outline-minor-mode 1))
(add-hook 'LaTeX-mode-hook 'turn-on-outline-minor-mode)

;; à documenter...
;; (require 'writegood-mode)
;; (global-set-key "\C-cg" 'writegood-mode)
;; gestion des images dans latex (à documenter...)
(setq LaTeX-includegraphics-read-file 'LaTeX-includegraphics-read-file-relative)
(require 'auto-complete-auctex)

;;de l'abnégation
;; (require 'disable-mouse)
;; (global-disable-mouse-mode)
;; (defvar too-hardcore-backspace)
;; (setq too-hardcore-backspace t)
;; (defvar too-hardcore-return)
;; (setq too-hardcore-return t)
;; (require 'hardcore-mode)
;; (global-hardcore-mode)

;;du test
;; (require 'workgroups2)
;; (workgroups-mode 1)
;; (require 'zoom "~/.emacs.d/elpa/zoom-20171110.715/zoom.el")
;; tabbar mode fait bugguer la visu de pdf
;; (require 'tabbar)
;; (tabbar-mode 1)
;; (setq-default cursor-type 'bar)

(provide 'init)
;;; init.el ends here
